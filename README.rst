############
Sudoku Maker
############

********
Overview
********

This project aims to create Sudoku puzzles with a unique solution using the backtracking algorithm.

It also contains a Sudoku solver and a tool to check the uniqueness of the solution.

**********
Dependency
**********

* C compiler

* (GNU) Make

***********
Quick start
***********

.. code-block:: console

   make all
   ./a.out 16384

The first argument of ``a.out`` (optional) is used as a random seed, which is used to change the outcome randomly.

The above commands give the following output:

.. code-block:: text

   seed: 16384
   answer:
    8  9  3  7  2  5  1  6  4
    1  7  4  9  6  3  5  2  8
    6  5  2  1  4  8  9  7  3
    9  8  7  6  3  4  2  5  1
    5  2  1  8  9  7  4  3  6
    4  3  6  5  1  2  7  8  9
    2  6  9  3  5  1  8  4  7
    7  1  5  4  8  6  3  9  2
    3  4  8  2  7  9  6  1  5
   question:
       9  3  7  2  5
             9           2
                            3
                   4     5  1
    5     1        7
    4                       9
             3     1  8  4
       1                    2
       4              6     5

*********
Reference
*********

* `Sudoku solving algorithms - Backtracking <https://en.wikipedia.org/wiki/Sudoku_solving_algorithms#Backtracking>`_


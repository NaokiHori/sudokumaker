#include "general.h"

// try to fill the value at the given position (index = indices[cnt]) randomly
static inline int fill_at (
    size_t * nsols,
    size_t * vals,
    const size_t nitems,
    const size_t * indices,
    const size_t cnt
) {
  if (nitems == cnt) {
    // new solution is found
    *nsols += 1;
    return 0;
  }
  const size_t index = indices[cnt];
  for (size_t n = 0; n < BOARD_SIZE; n++) {
    const size_t cand = n + 1;
    if (0 == check_and_assign(vals, index, cand)) {
      // valid, move forward
      fill_at(nsols, vals, nitems, indices, cnt + 1);
      // reset this cell and try to assign another value
      vals[index] = 0;
    }
  }
  // no valid applicant, this Sudoku does not have an answer
  // go back stack in the reversed order
  return 1;
}

// check number of solutions of the given sudoku
int count_nsols (
    size_t * vals,
    size_t * nsols
) {
  size_t nitems = 0;
  size_t * indices = NULL;
  if (0 != find_nonzero_indices(vals, &nitems, &indices)) {
    return 1;
  }
  // all filled, do nothing and report success
  if (0 == nitems) {
    // of course unique solution
    *nsols = 1;
    return 0;
  }
  int retval = fill_at(nsols, vals, nitems, indices, 0);
  my_free(indices);
  return retval;
}


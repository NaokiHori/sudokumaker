#include "general.h"

// try to fill the value at the given position (index = indices[cnt]) randomly
// NOTE: this function is invoked recursively,
//         whose maximum stack depth is nitems < BOARD_SIZE * BOARD_SIZE
static int fill_at (
    size_t * vals,
    const size_t nitems,
    const size_t * indices,
    const size_t cnt
) {
  if (nitems == cnt) {
    // already reached the end of the to-be-filled list
    // report success
    return 0;
  }
  const size_t index = indices[cnt];
  // candidates [1-9], random order to randomise the initial domain
  size_t cands[BOARD_SIZE] = {0};
  for (size_t n = 0; n < BOARD_SIZE; n++) {
    cands[n] = n + 1;
  }
  shuffle(BOARD_SIZE, cands);
  // try all candidates
  for (size_t n = 0; n < BOARD_SIZE; n++) {
    if (0 == check_and_assign(vals, index, cands[n])) {
      // valid, move forward
      if (0 == fill_at(vals, nitems, indices, cnt + 1)) {
        // done, report success
        return 0;
      } else {
        // failed at a later point, indicatng this candidate is N/A
        // reset this cell and try the other candidate
        vals[index] = 0;
      }
    }
  }
  // no valid applicant, this Sudoku has no answer
  // report failure to go back recursion
  return 1;
}

// try to find "one" solution
// NOTE: this function is used to initialise a given board, which might be empty
// the solution found first is returned and others (if any) are ignored
int find_one_solution (
    size_t * vals
) {
  // make a to-be-filled list (pick-up all non-zero cells)
  size_t nitems = 0;
  size_t * indices = NULL;
  if (0 != find_nonzero_indices(vals, &nitems, &indices)) {
    return 1;
  }
  // all filled, do nothing and report success
  if (0 == nitems) {
    return 0;
  }
  // try to fill vals at indices[0], called recursively
  int retval = fill_at(vals, nitems, indices, 0);
  my_free(indices);
  return retval;
}

